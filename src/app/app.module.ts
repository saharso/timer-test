import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CounterDashboardComponent } from './counter-dashboard/counter-dashboard.component';
import { TimeDisplayComponent } from './shared/components/time-display/time-display.component';
import { CounterHeaderComponent } from './counter-header/counter-header.component';
import { CounterControlsComponent } from './counter-controls/counter-controls.component';
import { CounterRecordsComponent } from './counter-records/counter-records.component';

@NgModule({
  declarations: [
    AppComponent,
    CounterDashboardComponent,
    TimeDisplayComponent,
    CounterHeaderComponent,
    CounterControlsComponent,
    CounterRecordsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
