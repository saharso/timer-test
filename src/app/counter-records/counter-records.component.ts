import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TimeData } from '../shared/models/time-data.model';

@Component({
  selector: 'app-counter-records',
  templateUrl: './counter-records.component.html',
  styleUrls: ['./counter-records.component.scss']
})
export class CounterRecordsComponent implements OnInit {
  
  @Input() timeList: TimeData[] = [];
  @Output() onRemoveFromList = new EventEmitter<TimeData>();

  constructor(){}
  
  ngOnInit(){}

  removeFromList( item ){
    this.onRemoveFromList.emit( item );
  }
}