import { Component, OnInit, Input } from '@angular/core';
import { TimeData } from '../shared/models/time-data.model';

@Component({
  selector: 'app-counter-header',
  templateUrl: './counter-header.component.html',
  styleUrls: ['./counter-header.component.scss']
})
export class CounterHeaderComponent implements OnInit {

  @Input() timeData: TimeData;
  
  constructor() { }

  ngOnInit() {}

}
