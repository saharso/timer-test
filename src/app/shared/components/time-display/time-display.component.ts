import { Component, OnInit, Input } from '@angular/core';
import { TimeData } from '../../models/time-data.model';

@Component({
  selector: 'app-time-display',
  templateUrl: './time-display.component.html',
  styleUrls: ['./time-display.component.scss']
})
export class TimeDisplayComponent implements OnInit {

  @Input() timeData: TimeData;
  prevSecond: 0;

  constructor() {}

  ngOnInit() {
    this.timeData
  }

  digitsSplitter( data: number ): string[] {
    const leadingZero: string = data < 10 ? '0': '';
    return ( leadingZero + data ).split( '' );
  }
  
}
