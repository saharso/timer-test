export class TimeData {
    seconds: number = 0;
    minutes: number = 0;
    hours: number = 0;
    _blink?: boolean = false;
}