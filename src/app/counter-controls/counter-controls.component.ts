import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter-controls',
  templateUrl: './counter-controls.component.html',
  styleUrls: ['./counter-controls.component.scss']
})
export class CounterControlsComponent implements OnInit {

  @Input() timerMotionState: boolean;
  @Output() onMotionToggle = new EventEmitter();
  @Output() onAddToList = new EventEmitter();
  @Output() onReset = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  toggleTimerMotionState(){
    this.timerMotionState = ! this.timerMotionState;
    this.onMotionToggle.emit( this.timerMotionState );
  }
  
  storeCurrentTime(){
    this.onAddToList.emit();
  }
  
  resetEntireState(){
    this.onReset.emit();
  }
  
}
