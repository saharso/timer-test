import { Component, OnInit } from '@angular/core';
import { TimeData } from '../shared/models/time-data.model';

@Component({
  selector: 'app-counter-dashboard',
  templateUrl: './counter-dashboard.component.html',
  styleUrls: ['./counter-dashboard.component.scss']
})
export class CounterDashboardComponent implements OnInit {

  interval;
  oneSecond: number = 1000;
  timeData: TimeData = new TimeData();
  timerMotionState: boolean = true;
  timeList: TimeData[] = [];

  constructor() { }

  ngOnInit() {
    this.timeData = JSON.parse( localStorage.getItem( 'timeData' ) ) || new TimeData();
    this.timeList = JSON.parse( localStorage.getItem( 'timeList' ) ) || [];
    this.playTimer();
  }
  ngOnDestroy(){
    this.interval = clearInterval( this.interval );
  }

  timer(){
    const t = this.timeData;
    t.seconds ++;
    t.seconds = t.seconds % 60;
    if( t.seconds === 0 ){
      t.minutes ++;
    }
    t.minutes = t.minutes % 60;
    if( t.minutes === 0 && t.seconds === 0 ){
      t.hours ++;
    }

    this.storeToBrowserMemory( 'timeData', this.timeData );
    this.blink();
  }

  toggleTimerMotionState(){
    this.timerMotionState = ! this.timerMotionState;
    this.timerMotionState ? this.playTimer() : this.pauseTimer();
  }

  storeCurrentTime(){
    const toExport = { ...this.timeData };
    delete toExport._blink;
    this.timeList.push( toExport );
    this.storeToBrowserMemory( 'timeList', this.timeList );
  }

  resetEntireState(){
    this.pauseTimer();
    this.timeData = new TimeData();
    this.timerMotionState = false;
    this.timeList = [];
    this.storeToBrowserMemory( 'timeData', this.timeData );
    this.storeToBrowserMemory( 'timeList', this.timeList );
  }
  
  removeFromList( item: TimeData ){
    this.timeList.splice( this.timeList.findIndex( e => e === item ), 1 );
    this.storeToBrowserMemory( 'timeList', this.timeList );
  }

  private blink() {
    this.timeData._blink = true;
    setTimeout( ()=> { delete this.timeData._blink }, 100 );
  }

  private playTimer(){
    this.interval = setInterval( this.timer.bind( this ), this.oneSecond );
  } 

  private pauseTimer(){
    this.interval = clearInterval( this.interval );
  }

  private storeToBrowserMemory( key: string, value: any ){
    localStorage.setItem( key, JSON.stringify( value ) );
  }

}
